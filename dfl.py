import sys
import os
import argparse
import toml
import json
from pathlib import Path
sys.path.append(os.path.abspath("./lib"))
from config import Config, ConfigType, Constraints, Evaluator, TrainingConfig



def main():
    process = ["show", "train"]

    # create the top-level parser
    parser = argparse.ArgumentParser(prog="$ python dfl.py")
    parser.add_argument("action", choices=process, help="What's next?")
    parser.add_argument("-c", "--config-file", help="Your custom config file.")
    # parser.add_argument("-m", "--model", help="The configuration for the model.")
    # parser.add_argument("-t", "--training", help="Name of the training stage.")
    # parser.add_argument("-ts", "--training-stage", help="Name of the training stage.")
    args = parser.parse_args()
    print(args)
    if args.action == "train":
        # Ways to load the entire config file of sections of it
        # A Config instance has got two fields that provide meta information:
        # type and file
        # The field dict provides the actual configuration

        #config = Config(ConfigType.TRAINING, "config.toml")
        #print(config.to_json())
        #config = Config(ConfigType.MODEL, "config.toml")
        #print(config.to_json())
        # >
        # {
        #     "file": "config.toml",
        #     "type": "model",
        #     "dict": {
        #         "description": "A SAEHD super model",
        #         "model_class_name": "SAEHD",
        #         "is_training": false,
        #         "is_exporting": false,
        #         "saved_models_path": "models",
        #         "training_data_src_path": "workspace/pretrain/data_src",
        #         "training_data_dst_path": "workspace/pretrain/data_dst",
        #         "pretraining_data_path": "FFHQ",
        #         "pretrained_model_path": "model",
        #         "no_preview": false,
        #         "cpu_only": false,
        #         "debug": false,
        #         "silent_start": false
        #     }
        # }
        # The following two examples load the entire configuration file (including the model and training section)
        #config = Config(ConfigType.ALL)
        #print(config.to_json())
        #config = Config(ConfigType.TRAINING)
        # print(config.to_json())
        # get_section returns a section of a Config instance as dictionary
        #model_dict = config.get_section(ConfigType.TRAINING)
        # The Config class provides a static pretty function
        # to print the dict or parts of it pretty.
        #print(Config.pretty(model_dict))
        # Convert an ALL Config type to a MODEL (or TRAINING) type
        #print(args.config_file)
        config = Config(ConfigType.ALL, args.config_file)
        # print(config.to_json())

        # Convert a Config into TrainingConfig
        # In this example the MODEL section of a ALL Config converts into a MODEL Config
        config = Config(ConfigType.ALL, args.config_file)
        t_config = TrainingConfig(config)
        # print(t_config.to_json() )

        # Sort the training stages by target iteration
        constraints = Constraints(config)
        iter = 7000
        loss = 0.25
        for c in constraints.constraints:
            print(f"Constraint expressions for: {c.stage_name}")
            if hasattr(c, "iter"):
                c_str = "iter " + c.iter
                print(f"({c.stage_name}): {c_str}")
            if hasattr(c, "loss"):
                c_str = "loss " + c.loss
                print(f"({c.stage_name}): {c_str}")
         

    elif (args.action == "show"):
        config = Config(ConfigType.ALL, args.config_file)
        print(config.to_json())

if __name__ == "__main__":
    main()
