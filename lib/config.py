import toml
import json
from operator import attrgetter
from enum import Enum
import copy

# A Section represents the first level of properties in a config file.
class ConfigType(Enum):
    ALL = "all"
    MODEL = "model"
    TRAINING = "training"


class Property(Enum):
    Stage = 'stage'
    Description = "description"
    TrainingDataSrcPath = "training_data_src_path"
    TrainingDataDstPath = "training_data_dst_path"
    PretrainingDataPath = "pretraining_data_path"
    PretrainedModelPath = "pretrained_model_path"
    NoPreview = "no_preview"
    ForceModelName = "force_model_name"
    ForceGpuIdxs = "force_gpu_idxs"
    CpuOnly = "cpu_only"
    SilentStart = "silent_start"
    ExecutePrograms = "execute_programs"
    Debug = "debug"
    Resolution = "resolution"
    FaceType = "face_type"
    ModelsOptOGpu = "models_opt_on_gpu"
    Architecture = "archi"
    AeDims = "ae_dims"
    EDims = "e_dims"
    DDims = "d_dims"
    DMaskDims = "d_mask_dims"
    MaskedTraining = "masked_training"
    EyesMouthPriority = "eyes_mouth_prio"
    UniformYaw = "uniform_yaw"
    AdaBelief = "adabelief"
    LrDropout = "lr_dropout"
    RandomWarp = "random_warp"
    true_face_power = "true_face_power"
    face_style_power = "face_style_power"
    bg_style_power = "bg_style_power"
    ct_mode = "ct_mode"
    clipgrad = "clipgrad"
    pretrain = "pretrain"
    autobackup_hour = "autobackup_hour"
    write_preview_history = "write_preview_history"
    target_iter = "target_iter"
    random_flip = "random_flip"
    batch_size = "batch_size"
    gan_power = "gan_power"
    gan_patch_size = "gan_patch_size"
    
class Config:
    # Loads the configuration file and returns a Config type
    # containing a dict depending on the section argument
    # None   : the entire configuration
    # 'model': the model
    # 'train': the training section, including all stages 

    def __init__(self, config_type="choices", file="config.toml"):
        choices = list(ConfigType)
        self.model_source = file
        if config_type != None:
             self.type = config_type.value
        else:
            self.type = ConfigType.ALL.value
        self.dict = Config.dict_from_file(config_type, file)
    
    def dict_from_file(config_type="choices", file="config.toml",) -> dict:
        choices = list(ConfigType)
        t = toml.load(file)
        if config_type == None or config_type == ConfigType.ALL:
            return t
        else:
            return t.get(config_type.value)

    def get_sections(self) -> []:
        sections = []
        for section in self.dict:
            sections.append(section)
        return sections

    # Given an ConfigType.ALL Config, 
    # returns a particular section [training, model] of it.
    def get_section(self, config_type="choices"):
        choices = list(ConfigType)
        if self.type == ConfigType.ALL.value:
            return self.dict.get(config_type.value)
        elif self.type == ConfigType.MODEL.value and config_type.value == ConfigType.MODEL.value:
            return self.dict
        elif self.type == ConfigType.TRAINING.value and config_type.value == ConfigType.TRAINING.value:
            return self.dict
        else:
            return json.dumps("{ 'error' : 'No section " + config_type.value + " available in ConfigType " + self.type + "' }" , indent=4)

    def to_json(self) -> str:
        return json.dumps(self.__dict__, indent=4)

    # static
    def pretty(dictionary) -> str:
        return json.dumps(dictionary, indent=4)
    
class TrainingConfig(Config):  
    def __init__(self, config):
        if config.type == ConfigType.ALL.value:
            self.model_source = "convert"
            self.type = ConfigType.TRAINING.value
            self.dict = config.get_section(ConfigType.TRAINING)
        elif config.type == ConfigType.TRAINING.value:
            print (json.dumps("{ 'error' : 'Cannot convert same source and target type.' }", indent=4) )
        else:
            print (json.dumps("{ 'error' : 'Cannot convert " + config.type + " to TrainingConfig. Does not contain [training] section.' }", indent=4) )      

class Constraint:
    def __init__(self, stage_name, dict):
        self.stage_name = stage_name
        iter = dict.get("iter")
        loss = dict.get("loss")
        if iter:
            self.iter = iter
        if loss:
            self.loss = loss
    

class Evaluator:
    def eval_than(expression) -> bool :
        print(f"Evaluating: {expression}")
        eval = False
        split = expression.split(" ")
        if len(split) == 2:
            than = split[0]
            loss_value = split[1]
            if than == ">" or than == "<" and float == type( float(loss_value) ):
                eval = True
            else:
                print("Error: Cannot evalute comparison. Must bei in form of 'loss': '<0.3'")
        return eval

# A collection of type Constraint
class Constraints(Config):
    def __init__(self, config):
        self.constraints = []
        training_section =  config.dict.get("training")
        for stage_name in training_section.keys():
            stage = training_section.get(stage_name)
            if "constraints" in stage.keys():
                self.constraints.append( Constraint(stage_name,  stage.get("constraints") ) )
    